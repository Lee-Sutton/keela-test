# Keela Test
- Implement a login and register page based on the One-UI theme

# Notes
- Converted one ui html pages to blaze templates
- Removed <script></script> tags from the html pages
- Refactored jquery logic into the register.js and login.js pages
- Used meteor accounts-password for managing user creation
- moved all css into main.css

# Areas for improvments/potential code smells
- jquery validation on the registration and sign in forms contain duplicated
code. It would be ideal if this code could be refactored into a function which could
be called in both modules and re-used in the future