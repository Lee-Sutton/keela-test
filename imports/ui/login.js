import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { OneUI } from './app';

import './login.html';

/**
 * @summary Form validation for the sign in page
 * @returns jquery validation object with the required input rules 
*/
var initValidationLogin = function(){
    return jQuery('.js-validation-login').validate({
        errorClass: 'help-block text-right animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        rules: {
            'login-username': {
                required: true,
                minlength: 3
            },
            'login-password': {
                required: true,
                minlength: 5
            }
        },
        messages: {
            'login-username': {
                required: 'Please enter a username',
                minlength: 'Your username must consist of at least 3 characters'
            },
            'login-password': {
                required: 'Please provide a password',
                minlength: 'Your password must be at least 5 characters long'
            }
        }
    });
};

/**
 * Displays the input error on the sign on page and directs the user
 * to correct the username or password
 * @param {Meteor.error} error - thrown when submitting the sign in form
 */
let displayError = (error) => {
  let validator = initValidationLogin();
  console.log(error);
  if (error.reason === "Incorrect password"){
    validator.showErrors({'login-password': error.reason});
  } else {
    validator.showErrors({'login-username': error.reason});
  }
};

/**
 * Submits the login information and displays any errors on the form
 */
Template.login.events({
  'submit form': (event) => {
    event.preventDefault();
    let username = $('[name=login-username]').val();
    let password = $('[name=login-password]').val();
    Meteor.loginWithPassword({username}, password, 
    (error) => {
      if (error) {
        console.log(error.reason);
        displayError(error);
      }else {
        Router.go('/home');
        console.log('Logged in');
      }
    });
  }
});

// Form validation on the client side
Template.login.onRendered(() => {
  OneUI.init();
  initValidationLogin();
});