import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { OneUI } from './app';
import './register.html';

/**
 * @summary Initializes the form validator on the client side. Sets requirements
 * for the registration form inputs
 * @returns jquery validation object with the required input rules
*/
var initValidationRegister = function(){
    return jQuery('.js-validation-register').validate({
        errorClass: 'help-block text-right animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        rules: {
            'register-username': {
                required: true,
                minlength: 3
            },
            'register-email': {
                required: true,
                email: true
            },
            'register-password': {
                required: true,
                minlength: 5
            },
            'register-password2': {
                required: true,
                equalTo: '#register-password'
            },
            'register-terms': {
                required: true
            }
        },
        messages: {
            'register-username': {
                required: 'Please enter a username',
                minlength: 'Your username must consist of at least 3 characters'
            },
            'register-email': 'Please enter a valid email address',
            'register-password': {
                required: 'Please provide a password',
                minlength: 'Your password must be at least 5 characters long'
            },
            'register-password2': {
                required: 'Please provide a password',
                minlength: 'Your password must be at least 5 characters long',
                equalTo: 'Please enter the same password as above'
            },
            'register-terms': 'You must agree to the service terms!'
        }
    });
};

/**
 * Displays user creation errors on the registration form
 * @param {Meteor.error} error 
 */
let displayError = (error) => {
  let validator = initValidationRegister();
  if (error.reason === "Email already exists.") {
    validator.showErrors({'register-email': error.reason});
  } else {
    validator.showErrors({'register-username': error.reason});
  }
};

// Jquery validation for the client
Template.register.onRendered(() => {
  initValidationRegister();
  OneUI.init();
});

/**
 * Creates an account for the user with the input credentials. If any 
 * errors occur during the form submission, the errors are displayed on
 * the form
 */
Template.register.events({
  'submit form': (event) => {
    event.preventDefault();
    let username = $('[name=register-username]').val();
    let email = $('[name=register-email]').val();
    let password = $('[name=register-password]').val();
    let password2 = $('[name=register-password2]').val();
    console.log('creating account');
    Accounts.createUser({
      username,
      email,
      password
    }, (error) => {
      if (error) {
        displayError(error)
      } else {
        Router.go('/home');
      }
    });
  }
});