/**
 * Account creation rules
*/
export const rules = {
  'register-username': {
    required: true,
    minlength: 3
  },
  'register-email': {
    required: true,
    email: true
  },
  'register-password': {
    required: true,
    minlength: 5
  },
  'register-password2': {
    required: true,
    equalTo: '#register-password'
  },
  'register-terms': {
    required: true
  }
};

/**
* Validates new users before storing in the server
*/
Accounts.validateNewUser((user) => {
  if (user.username && user.username.length >= 3) {
    return true;
  } else {
    throw new Meteor.Error(403, 'Username must have at least 3 characters');
  }
});

